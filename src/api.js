const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

const createTagList = async (pings) => {
  const staleTime = Date.now() - process.env.TAGS_FREQUENCY_STALE_TIME_IN_MILLISECONDS;
  const tagList = await pings.aggregate([
    { $match: { date: { $gte: staleTime } } },
    { $project: { tags: true } },
    { $unwind: "$tags" },
    { $group: { _id: "$tags", count: { $sum: 1 } } }
  ]).toArray();

  return tagList;
}

const isTagRegistryExpired = (tagRegistry) => !tagRegistry || tagRegistry.expiresAt < Date.now();

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  const db = mongoClient.db("tags");

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  api.get("/tagnow", async (req, res) => {
    const pings = db.collection("pings");
    const tagLists = db.collection("tagLists");

    const tagRegistry = await tagLists.findOne({});
    let tagList;
    if (isTagRegistryExpired(tagRegistry)) {
      tagList = await createTagList(pings);
      const expiresAt = Date.now() + process.env.TAGS_FREQUENCY_STALE_TIME_IN_MILLISECONDS;
      if (!tagRegistry) {
        await tagLists.insertOne({
          tags: tagList,
          expiresAt 
        });
      } else {
        await tagLists.updateOne({}, {$set: {
          tags: tagList,
          expiresAt 
        }});
      }
    }
    else {
      tagList = tagRegistry.tags;
    }

    res.json({
      tags: tagList,
    });
  });

  return api;
};
